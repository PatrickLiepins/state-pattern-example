package de.liepins.statepattern.zustand;

import de.liepins.statepattern.controller.IndexBean;
import de.liepins.statepattern.helper.MessageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Zustand {

    protected final IndexBean indexBean;
    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    protected String message;

    protected Zustand(final IndexBean indexBean) {
        this.indexBean = indexBean;
    }

    public abstract void muenzeEinwerfen();

    public abstract void muenzeAuswerfen();

    public abstract void griffDrehen();

    public String getMessage() {
        return message;
    }
}