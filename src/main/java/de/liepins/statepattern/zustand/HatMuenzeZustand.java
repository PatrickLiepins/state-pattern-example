package de.liepins.statepattern.zustand;

import de.liepins.statepattern.controller.IndexBean;
import de.liepins.statepattern.helper.MessageUtil;

public class HatMuenzeZustand extends Zustand {

    public HatMuenzeZustand(final IndexBean indexBean) {
        super(indexBean);
        this.message = ZustandMessages.HAT_MUENZE_ZUSTAND_TITEL;
    }

    @Override
    public void muenzeEinwerfen() {
        MessageUtil.addWarn(ZustandMessages.MUENZE_EINWERFEN_MUENZE_VORHANDEN);
    }

    @Override
    public void muenzeAuswerfen() {
        MessageUtil.addInfo(ZustandMessages.MUENZE_AUSWERFEN);
        indexBean.setZustand(indexBean.getKeineMuenze());
    }

    @Override
    public void griffDrehen() {
        MessageUtil.addInfo(ZustandMessages.GRIFF_DREHEN);
        kugelAusgeben();
    }

    private void kugelAusgeben() {
        MessageUtil.addInfo(ZustandMessages.KUGEL_AUSGEBEN);
        if (indexBean.getKugelAnzahl() != 0) {
            indexBean.setKugelAnzahl(indexBean.getKugelAnzahl() -1);
        }
        if (indexBean.getKugelAnzahl() > 0) {
            indexBean.setZustand(indexBean.getKeineMuenze());
        } else {
            MessageUtil.addFatal(ZustandMessages.KUGEL_AUSGEBEN_KEINE_KUGELN);
            indexBean.setZustand(indexBean.getAusverkauft());
        }
    }

}
