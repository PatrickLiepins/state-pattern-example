package de.liepins.statepattern.zustand;

public class ZustandMessages {

    public static final String HAT_MUENZE_ZUSTAND_TITEL = "Bitte Griff drehen.";
    public static final String KEINE_MUENZE_ZUSTAND_TITEL = "Bitte Münze einwerfen.";
    public static final String VERKAUFT_ZUSTAND_TITEL = "Griff wurde gedreht";
    public static final String AUSVERKAUF_ZUSTAND_TITEL = "Leider ausverkauft";

    public static final String MUENZE_EINWERFEN = "Sie haben eine Münze eingeworfen.";
    public static final String MUENZE_EINWERFEN_MUENZE_VORHANDEN = "Sie können keine weiter Münze einwerfen.";
    public static final String MUENZE_EINWERFEN_AUSVERKAUFT = "Sie können keine Münze einwerfen.";

    public static final String MUENZE_AUSWERFEN = "Münze wird ausgeworfen.";
    public static final String MUENZE_AUSWERFEN_BEREITS_VERKAUFT = "Zu spät sie haben den Griff schon gedreht";
    public static final String MUENZE_AUSWERFEN_KEINE_MUENZE = "Keine Münze vorhanden.";

    public static final String GRIFF_DREHEN = "Sie haben den Griff gedreht...";
    public static final String GRIFF_DREHEN_NICHT_MOEGLICH = "Sie können den Griff nicht drehen.";
    public static final String GRIFF_DREHEN_KEINE_MUENZE = "Sie haben keine Münze eingeworfen";
    public static final String GRIFF_DREHEN_BEREITS_VERKAUFT = "Auch wenn Sie zweimal drehen, bekommen Sie keine zweite kugel.";

    public static final String KUGEL_AUSGEBEN = "Kugel wirs ausgegeben....";
    public static final String KUGEL_AUSGEBEN_KEINE_KUGELN = "Keine Kugeln mehr. Bitte Auffüllen!";

}
