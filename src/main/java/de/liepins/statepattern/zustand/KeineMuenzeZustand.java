package de.liepins.statepattern.zustand;

import de.liepins.statepattern.controller.IndexBean;
import de.liepins.statepattern.helper.MessageUtil;

public class KeineMuenzeZustand extends Zustand {

    public KeineMuenzeZustand(final IndexBean indexBean) {
        super(indexBean);
        this.message = ZustandMessages.KEINE_MUENZE_ZUSTAND_TITEL;
    }

    @Override
    public void muenzeEinwerfen() {
        MessageUtil.addInfo(ZustandMessages.MUENZE_EINWERFEN);
        indexBean.setZustand(indexBean.getHatMuenzeZustand());
    }

    @Override
    public void muenzeAuswerfen() {
        MessageUtil.addWarn(ZustandMessages.MUENZE_AUSWERFEN_KEINE_MUENZE);
    }

    @Override
    public void griffDrehen() {
        MessageUtil.addWarn(ZustandMessages.GRIFF_DREHEN_KEINE_MUENZE);
    }

}
