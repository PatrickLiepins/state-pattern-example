package de.liepins.statepattern.zustand;

import de.liepins.statepattern.controller.IndexBean;
import de.liepins.statepattern.helper.MessageUtil;

public class VerkauftZustand extends Zustand {

    public VerkauftZustand(IndexBean indexBean) {
        super(indexBean);
        this.message = ZustandMessages.VERKAUFT_ZUSTAND_TITEL;
    }

    @Override
    public void muenzeEinwerfen() {
        MessageUtil.addWarn(ZustandMessages.MUENZE_EINWERFEN_MUENZE_VORHANDEN);
    }

    @Override
    public void muenzeAuswerfen() {
        MessageUtil.addError(ZustandMessages.MUENZE_AUSWERFEN_BEREITS_VERKAUFT);
    }

    @Override
    public void griffDrehen() {
        MessageUtil.addWarn(ZustandMessages.GRIFF_DREHEN_BEREITS_VERKAUFT);
    }


}
