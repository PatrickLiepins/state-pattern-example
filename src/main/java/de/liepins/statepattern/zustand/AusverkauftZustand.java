package de.liepins.statepattern.zustand;

import de.liepins.statepattern.controller.IndexBean;
import de.liepins.statepattern.helper.MessageUtil;

public class AusverkauftZustand extends Zustand {

    public AusverkauftZustand(IndexBean indexBean) {
        super(indexBean);
        this.message = ZustandMessages.AUSVERKAUF_ZUSTAND_TITEL;
    }

    @Override
    public void muenzeEinwerfen() {
        MessageUtil.addInfo(ZustandMessages.MUENZE_AUSWERFEN);
    }

    @Override
    public void muenzeAuswerfen() {
        MessageUtil.addError(ZustandMessages.MUENZE_AUSWERFEN_KEINE_MUENZE);
    }

    @Override
    public void griffDrehen() {
        MessageUtil.addError(ZustandMessages.GRIFF_DREHEN_NICHT_MOEGLICH);
    }
}
