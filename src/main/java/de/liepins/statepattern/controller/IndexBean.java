package de.liepins.statepattern.controller;

import de.liepins.statepattern.zustand.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

@Controller
@Scope("session")
public class IndexBean {

    private Zustand keineMuenze = new KeineMuenzeZustand(this);
    private Zustand hatMuenze = new HatMuenzeZustand(this);
    private Zustand ausverkauft = new AusverkauftZustand(this);


    private int kugelAnzahl = 5;

    private Zustand zustand;

    public IndexBean() {
        if (kugelAnzahl > 0) {
            this.zustand = keineMuenze;
        } else {
            this.zustand = ausverkauft;
        }
    }

    public String getTitle() {
        return zustand.getMessage();
    }

    public void setZustand(final Zustand zustand) {
        this.zustand = zustand;
    }

    public Zustand getZustand() {
        return zustand;
    }

    public Zustand getHatMuenzeZustand() {
        return hatMuenze;
    }

    public Zustand getKeineMuenze() {
        return keineMuenze;
    }

    public int getKugelAnzahl() {
        return kugelAnzahl;
    }

    public void setKugelAnzahl(int kugelAnzahl) {
        this.kugelAnzahl = kugelAnzahl;
    }

    public Zustand getAusverkauft() {
        return ausverkauft;
    }
}
